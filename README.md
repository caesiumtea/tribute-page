# Red panda tribute page

Simple web page (HTML/CSS only) with photos and fun facts about red pandas. This page was created for the ["tribute page" project](https://www.freecodecamp.org/learn/2022/responsive-web-design/build-a-tribute-page-project/build-a-tribute-page) in the [freeCodeCamp](https://www.freecodecamp.org/learn/) Responsive Web Design course.

[View live site on GitLab Pages](https://caesiumtea.gitlab.io/tribute-page/)

[![Hippocratic License HL3-LAW-MIL-SV](https://img.shields.io/static/v1?label=Hippocratic%20License&message=HL3-LAW-MIL-SV&labelColor=5e2751&color=bc8c3d)](https://firstdonoharm.dev/version/3/0/law-mil-sv.html)

## Process
The project was designed to give practice with responsive design and semantic page layout, with the instructions calling for certain page sections and a large top image that resizes responsively. I extended the project by adding the grid gallery, because I also wanted to practice with CSS grid. 

During the process of making this page, I also ended up writing [this dev.to tutorial](https://dev.to/caesiumtea/mini-css-trick-emoji-page-dividers-with-the-content-property-3k42) about how I made the emoji page dividers here.

### Methods used
- Semantic HTML 
  - `section` and `article` tags
  - accessibility focused design (e.g., textual heading for every section)
  - description list
- CSS grid
- Responsive design
  - media query
  - responsive images and grid blocks
  - responsive font sizing
- Google fonts
- `::after` pseudo-selector with `content` property

### Design
Much of the page design was implied or dictated by the project instructions, which said to use a large responsive image at the top of the page and called for certain text sections. (I actually did veer off a little, since they intended you to make a tribute to a _person_ and asked for a bio section, which I replaced with "traits" when I picked an animal species as my topic instead.) The overall layout was expected to be pretty straightforward, and I didn't feel inspired to change that, even though I ended up kind of disappointed by how "plain" the layout feels.

The gallery was entirely my own design, though. When planning out my content, there were some facts I wanted to include but didn't know how to tie together into paragraphs or lists. Then I thought about the way magazines often present a big image on one page and a big pull quote on the facing page, and how they convey a sense of connection very implicitly. It gave me the idea to put a series of images side by side with facts written in big, pull-quote-like text--a way for each fact to feel isolated into its own context, but still feel like part of something. 

I wasn't thinking in HTML/CSS terms at first; I was just focusing on those couple of graphic design concepts. I was surprised by how complex it ended up being to translate the image in my head into real code. I did know I wanted to experiment with CSS grid, and knew that I wanted the gallery to always be a completely filled rectangle, and not let the last line turn out shorter than the others. The page was meant to be responsive, so I planned to use the grid's ability to automatically add and remove columns according to page width. However, I also wanted to make sure that it would always alternate between photo and fact, and never have two photo cards or two fact cards directly next to each other, either horizonally _or_ vertically. That meant that there would always have to be an odd number of columns, so I couldn't let them just add and remove automatically. The other problem with auto clomuns is that it would sometimes create a non-full bottom row. Instead I would have to write a separate media query for each possible number of columns, and pick a specific number of gallery cards that would always split into full rows. I decided on one single column for mobile-size views, and _wanted_ to allow both 3 and 5 for desktop--but that would require 15 cards to make the bottom row full on all layouts, and 15 just felt like too much. So I settled for just doing a single column on mobile and three columns on tablet/desktop.

## Learning
This project was my first freeform practice with CSS grid, and also my first time using description lists. I also learned about responsive text size for the first time, which I hadn't even considered until running into issues here with text sizing.

### Challenges
- At first, my media query didn't seem to be working and I couldn't tell why. Turns out it was because I had put the media query at the top of the CSS file, so it was never applied because the generic rule that came later down in the file would always override it. I actually couldn't figure out on my own what was going on, though, and ended up needing to ask my [bootcamp](https://badwebsite.club/) pals for help debugging. This is how I learned about the importance of line order!
- Looking at my grid at different resolutions, I realized that the responsiveness caused issues with my text, which was originally sized just in rem. On narrow screens the text was far too big for the cards and got cut off; on wide screens it looked humorously tiny. [Carlos](https://github.com/car2t) pointed me to the solution by telling me about how you can size text in vi instead!
- The biggest issue I ran into was a bug where, after using flexbox on the card content to vertically center it, the top of the text would be cut off. Turns out this was due to a conflict between flexbox's wrap properties and the normal overflow properties. All I had to do was turn on `flex-wrap` to prevent the weird clipping... but I definitely wouldn't have figured that out on my own!

### Lessons to pass on
- Media queries should be placed at the end of the file, because they're the one that's supposed to override the general case
- When your layout resizes responsively, make sure to think about how it relates to the text size too
- The flexbox wrap settings affect the way overflow displays

## Further development
If you see any error in the web page, or even just want to suggest any way my code could be improved, please do submit a GitLab issue or contact me with one of the methods listed under Author. If you see that I've gotten something wrong in the #Learning section of this document, I would be glad for any corrections about that too.

For the most part, I consider this page completed, but here are still a few ideas that I'm curious to maybe explore later.

- Is there a way I could have maintained my "never two facts or two photos next to each other" design in a grid layout that could alternate (based on screen width) between an even number of columns? Doing so would require that the actual _order of elements_ in the HTML document be able to change depending on the . I suspect this could be done with Javascript.
- Try limiting the max width or size of some elements for very large/wide displays
- Try adding a 5-column layout for large screens (and adding 3 more cards to make sure the bottom row is still filled)
- Maybe a small border along the left and right outside edges of the page would help pull the design together and make it feel less "plain"; maybe even a dotted border?
- How playful/serious do I want the vibe to be, and what can I change to get the desired effect?
- A navigation bar at the top would probably be a good addition

## Author
Hey, I'm **caesiumtea**, AKA Vance! Feel free to contact me with any feedback.

- [Website and social links](https://caesiumtea.glitch.me/)
- [@caesiumtea_dev on Twitter](https://www.twitter.com/caesiumtea_dev)
- [@entropy@mastodon.social](https://mastodon.social/@entropy)

## Acknowledgments
[Carlos](https://github.com/car2t) was a big help to me during this project! He told me about using `vi` units to create responsive text, which helped me solve how to make the text fit inside the cards, but _also_ taught me to consider the case where my website could be viewed in a language that's not left-to-right. If I recall correctly, he might've also helped debug the media query.

Thanks also to Tzu for telling me how to fix the overflow issues by setting the flex wrap!

Please see the Sources section at the bottom of the page for information sources used to write the content.

Includes photos from Unsplash users [Alexas_Fotos](https://unsplash.com/@alexas_fotos), [Ales Krivec](https://unsplash.com/@aleskrivec), [Thomas Bonometti](https://unsplash.com/@bonopeppers), [Joshua J. Cotten](https://unsplash.com/@jcotten), [Mohamed Elsayed](https://unsplash.com/@_melsayed), [Diana Parkhouse](https://unsplash.com/@ditakesphotos), and [Valentin Petkov](https://unsplash.com/@thefreak1337).

This readme is based on a template by [Frontend Mentor](https://www.frontendmentor.io/). 